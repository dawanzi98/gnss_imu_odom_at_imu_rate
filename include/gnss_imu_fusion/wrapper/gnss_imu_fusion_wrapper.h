#pragma once
#include "ros/ros.h"
#include "sensor_msgs/Imu.h"

#include "gnss_imu_fusion/fusion/gnss_imu_fusion.h"
#include "sensor_msgs/NavSatFix.h"
class GnssImuFusionWrapper
{
private:
    ros::Subscriber gnss_sub;
    ros::Subscriber imu_sub;
    ros::Publisher path_pub;
    ros::Publisher odom_pub;
    std::shared_ptr<GNSSImuFusion> fusion;

    int hz_mode = 0;

public:
    GnssImuFusionWrapper(ros::NodeHandle &nh);
    ~GnssImuFusionWrapper();
    void imu_cb(const sensor_msgs::ImuPtr &msg);

    void normal_gnss(const sensor_msgs::NavSatFixPtr&msg);
    void pub_msgs(SlamCraft::ESKF::State_18 state,double time);
};
