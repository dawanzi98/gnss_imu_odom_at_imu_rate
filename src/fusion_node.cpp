
#include "gnss_imu_fusion/wrapper/gnss_imu_fusion_wrapper.h"

int main(int argc, char *argv[])
{
    ros::init(argc,argv,"gnss_imu_fusion_node");
    ros::NodeHandle nh;
    GnssImuFusionWrapper w(nh);
    ros::spin();
    return 0;
}

