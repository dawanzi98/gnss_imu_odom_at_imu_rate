#pragma once
#include <Eigen/Dense>
namespace SlamCraft
{
    Eigen::Matrix3d  skew_symmetric(const Eigen::Vector3d &so3);
    Eigen::Matrix3d so3Exp(const Eigen::Vector3d &so3 );
    Eigen::Vector3d SO3Log(const Eigen::Matrix3d&SO3 );
} // namespace SlamCraft

