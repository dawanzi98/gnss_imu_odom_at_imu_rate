#pragma once
#include "Eigen/Dense"
#include <iostream>
namespace SlamCraft
{
    class IMU
    {
    public:
        Eigen::Vector3d acc = Eigen::Vector3d::Zero();
        Eigen::Vector3d gro = Eigen::Vector3d::Zero();
        Eigen::Quaterniond rot = Eigen::Quaterniond::Identity();
        double stamp = 0.0;//s
        void clear(){
            acc = Eigen::Vector3d::Zero();
            gro = Eigen::Vector3d::Zero();
            stamp = 0;
        }
        IMU operator + (const IMU& imu){
            IMU res;
            res.acc = this->acc+imu.acc;
            res.gro = this->gro+imu.gro;
            return res;
        }
        IMU operator *(double k){
            IMU res;
            res.acc = this->acc *k;
            res.gro = this->gro *k;
            res.stamp = this->stamp;
            return res;
        }
        IMU  operator/(double k){
            IMU res;
            res.acc = this->acc /k;
            res.gro = this->gro /k;
            res.stamp = this->stamp;
            return res;
        }
        friend std::ostream & operator<<(std::ostream& ostream,const IMU &imu){
            ostream<<"imu_time: "<<imu.stamp<<" ms | imu_acc: "<<imu.acc.transpose()<<" | imu_gro: "<< imu.gro.transpose() ;
            return ostream;
        }
    };

} // namespace SlamCraft

